const EventEmitter = require('events');
const MyLogger = require('./MyLogger');
const myDate = require('./customModule')

setTimeout(() => {
    console.log("inside timeout")
}, 2000)

const myLogger = new MyLogger();

myLogger.on('messageLogged', () => {
    console.log("Message logged")
})

myLogger.log("event emitter extends called.");

console.log("Hello node.js", myDate.myDate())
console.log("Program ended")