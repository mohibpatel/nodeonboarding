const fs = require('fs');

fs.writeFile('xyz.txt', 'Hello from Xyz.txt. Written by fs', (err, data) => {
    if (err) console.log("Something went wrong ", err);
    console.log("Data writing successful")
})

fs.readFile('xyz.txt', (err, data) => {
    if (err) console.log("Something went wrong ", err);
    console.log("File content --> ", data.toString())
})

console.log("Program ended.")