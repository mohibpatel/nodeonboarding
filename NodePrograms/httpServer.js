const http = require('http');

http.createServer((req, res) => {
    console.log("Request received")
    // console.log("Req", res)
    res.writeHead(200, { 'content-type': 'text/plain' })
    res.write("Connected with http server")
    res.end()
}).listen(8081)

console.log("Server running at 8081")