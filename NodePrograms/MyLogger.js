const EventEmitter = require('events');

class MyLogger extends EventEmitter {
    log(message) {
        console.log("MyLogger.js Log --> " + message);

        this.emit('messageLogged');
    }
}

module.exports = MyLogger